#include <iostream>
#include <stack>
#include <queue>
#include <fstream>
#include <vector>

// Stacks structure
//    [G] [R]                 [P]    
//    [H] [W]     [T] [P]     [H]    
//    [F] [T] [P] [B] [D]     [N]    
//[L] [T] [M] [Q] [L] [C]     [Z]    
//[C] [C] [N] [V] [S] [H]     [V] [G]
//[G] [L] [F] [D] [M] [V] [T] [J] [H]
//[M] [D] [J] [F] [F] [N] [C] [S] [F]
//[Q] [R] [V] [J] [N] [R] [H] [G] [Z]
// 1   2   3   4   5   6   7   8   9 

std::stack<char>* init_stacks() {
    std::stack<char>* result = new std::stack<char>[9]; 
    int values_len = 9;
    std::string values[] = {
        "QMGCL",
        "RDLCTFHG",
        "VJFNMTWR",
        "JFDVQP",
        "NFMSLBT",
        "RNVHCDP",
        "HCT",
        "GSJVZNHP",
        "ZFHG",
    };
    
    for(int i = 0; i < values_len; i++) {
        std::stack<char> stck;
        int len = values[i].length();

        for(int j = 0; j < len; j++) {
            stck.push(values[i][j]);
        }

        result[i] = stck;
    }

    return result;
}

std::vector<std::string> split(std::string s, char delim) {
    std::vector<std::string> result;
    std::string s_slice = "";
    int s_len = s.length();

    for(int i = 0; i < s_len; i++) {
        if(s[i] == delim) {
            result.push_back(s_slice);
            s_slice = "";
            continue;
        }
    
        s_slice += s[i];
    }

    result.push_back(s_slice);

    return result;
}

void move(int count, std::stack<char>& from, std::stack<char>& to) {
    for(int i = 0; i < count; i++) {
        char top = from.top();
        from.pop();
        to.push(top);
    }
}


// PART TWO
void move_p2(int count, std::stack<char>& from, std::stack<char>& to) {
    char tmp[count];

    for(int i = 0; i < count; i++) {
        tmp[i] = from.top();
        from.pop();
    }

    for(int i = count - 1; i >= 0; --i) {
        to.push(tmp[i]);
    }
}

int main(int argc, char** argv) {
    std::stack<char>* stacks = init_stacks();     
    std::fstream file;
    std::string line;

    file.open("input.txt", std::ios::in);
    

    while(getline(file, line)) {
        std::vector<std::string> splitted = split(line, ' ');
        int count = std::stoi(splitted[1]);
        int from_i = std::stoi(splitted[3]) - 1;
        int to_i = std::stoi(splitted[5]) - 1;
        
        //Part one
        //move(count, stacks[from_i], stacks[to_i]);
        //

        move_p2(count, stacks[from_i], stacks[to_i]);
    }

    for(int i = 0; i < 9; i++) {
        std::cout << stacks[i].top();
    }

    file.close();
    
    return 0;
}
