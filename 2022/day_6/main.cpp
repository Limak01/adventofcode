#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

//Count of distinct chars in part one and two
#define DIST_CHARS_P1 4
#define DIST_CHARS_P2 14

int main(int argc, char**argv) {
    std::fstream file;
    std::string signal;

    file.open("input.txt", std::ios::in);
    getline(file, signal);
    file.close();

    int len = signal.length();
    

    for(int i = 0; i < len; i++) {
        std::vector<char> buff;

        for(int j = i; j < i + DIST_CHARS_P2; j++) {
            if(j < len && std::find(buff.begin(), buff.end(), signal[j]) == buff.end()) {
                buff.push_back(signal[j]);
            }
        }
        
        if(buff.size() == DIST_CHARS_P2) {
            std::cout << i + DIST_CHARS_P2;
            break;
        }
    }

    return 0;
}
