#include <iostream>
#include <fstream>
#include <unordered_map>

#define WIN 6
#define DRAW 3

int get_score(char me ,char opponent, std::unordered_map<char, std::pair<char, char>>& results, std::unordered_map<char, int>& scores) {
    if(results[opponent].first == me) {
        return WIN + scores[me];
    }

    if (results[opponent].second == me) {
        return scores[me];
    } 

    return DRAW + scores[me];
}

int get_score_part_two(char me ,char opponent, std::unordered_map<char, std::pair<char, char>>& results, std::unordered_map<char, int>& scores, std::unordered_map<char, char>& values) {
    if(me == 'X') {
        return scores[results[opponent].second];
    }

    if (me == 'Z') {
        return WIN + scores[results[opponent].first];
    } 

    return DRAW + scores[values[opponent]];
}

int main(int argc, char** argv) {
    std::fstream file;
    std::string line;
    std::unordered_map<char, std::pair<char, char>> results;
    std::unordered_map<char, int> scores;
    std::unordered_map<char, char> values;
    int result = 0;

    results['A'] = {'Y', 'Z'};
    results['B'] = {'Z', 'X'};
    results['C'] = {'X', 'Y'};

    scores['X'] = 1;
    scores['Y'] = 2;
    scores['Z'] = 3;

    values['A'] = 'X';
    values['B'] = 'Y';
    values['C'] = 'Z';

    file.open("input.txt", std::ios::in);
    
    while(getline(file, line)) {
        char opponent = line[0];
        char me = line[2];

        result += get_score_part_two(me, opponent, results, scores, values);
    }

    std::cout << result << std::endl;
    
    file.close();
    return 0;
}
