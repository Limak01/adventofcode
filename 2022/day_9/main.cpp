#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <math.h>

struct Vector2 {
    int x;
    int y;
};

const std::vector<Vector2> DIRS = {
    {1, 0},
    {-1, 0},
    {0, 1},
    {0, -1},
    {1, 1},
    {-1, 1},
    {-1, -1},
    {1, -1},
};

std::vector<std::string> split(std::string s, char delim) {
    int s_len = s.length();
    std::string tmp = "";
    std::vector<std::string> result;

    for(int i = 0; i < s_len; i++) {
        if(s[i] == delim) {
            result.push_back(tmp);
            tmp = "";
            continue;
        }

        tmp += s[i];
    }

    result.push_back(tmp);

    return result;
}

bool find(std::vector<Vector2>& vec, Vector2 el) {
    for(Vector2 v: vec) {
        if(v.x == el.x && v.y == el.y) {
            return true;
        }
    }

    return false;
}

float distance(Vector2 head, Vector2 tail) {
    float dist = std::pow((head.x - tail.x), 2) + std::pow((head.y - tail.y), 2);

    return std::sqrt(dist);
}

//Part one
void mv(Vector2& head, Vector2& tail, Vector2 direction) {
    
    head.x += direction.x;
    head.y += direction.y;
    
    if(distance(head, tail) > std::sqrt(2.0)) {
        Vector2 dir;

        for(Vector2 v: DIRS) {
            Vector2 tmp;
            tmp.x = tail.x + v.x;
            tmp.y = tail.y + v.y;

            if(distance(head, tmp) == 1) {
                tail.x += v.x;
                tail.y += v.y;
                break;
            }
        }

    }
}

//Part two
void mv_2(std::vector<Vector2>& rope, Vector2 direction) {
    int rope_len = rope.size(); 
    rope[0].x += direction.x;
    rope[0].y += direction.y;

    for(int i = 0; i < rope_len; i++) {
        if(i + 1 >= rope_len) break;

        if(distance(rope[i], rope[i + 1]) > std::sqrt(2.0)) {
            Vector2 dir;
            float smallest = 10.f;
            
            //Find smallest distance between two points
            for(Vector2 v: DIRS) {
                Vector2 tmp;
                tmp.x = rope[i + 1].x + v.x;
                tmp.y = rope[i + 1].y + v.y;

                if(distance(rope[i], tmp) < smallest) {
                    smallest = distance(rope[i], tmp);
                }
            }
            
            for(Vector2 v: DIRS) {
                Vector2 tmp;
                tmp.x = rope[i + 1].x + v.x;
                tmp.y = rope[i + 1].y + v.y;

                if(distance(rope[i], tmp) == smallest) {
                    rope[i+1].x += v.x;
                    rope[i+1].y += v.y;
                    break;
                }
            }
        }
    }
}

int main(int argc, char** argv) {
    std::fstream file("input.txt", std::ios::in);
    std::string line;
    std::vector<Vector2> visited;
    std::unordered_map<std::string, Vector2> directions;

    directions["U"] = { 0, 1 };
    directions["D"] = { 0, -1 };
    directions["L"] = { -1, 0 };
    directions["R"] = { 1, 0 };
    Vector2 head = { 0, 0 }, tail = { 0, 0 }; 

    // For part two
    std::vector<Vector2> rope = {
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 },
        { 0, 0 }
    };

    int rope_len = rope.size();

    while(getline(file, line)) {
        std::vector<std::string> splitted = split(line, ' ');

        for(int i = 0; i < std::stoi(splitted[1]); i++) {
            mv_2(rope, directions[splitted[0]]);

            if(!find(visited, rope[rope_len - 1])) {
                visited.push_back(rope[rope_len - 1]);
            }
        }
    }

    std::cout << visited.size();
    
    file.close();
    return 0;
}
