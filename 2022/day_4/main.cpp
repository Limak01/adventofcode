#include <iostream>
#include <fstream>

std::string* split(std::string s, char deli) {
    std::string* result = new std::string[2];
    int start = 0;
    int deli_i = s.find(deli);
    int end = s.length();

    result[0] = s.substr(start, deli_i);
    result[1] = s.substr(deli_i + 1, end);

    return result;
}

int main(int argc, char** argv) {
    std::fstream file;
    std::string line;
    int result = 0;

    file.open("input.txt", std::ios::in);
    
    while(getline(file, line)) {
        std::string* line_splitted = split(line, ',');
        std::string* first_elf = split(line_splitted[0], '-');
        std::string* second_elf = split(line_splitted[1], '-');

        int elf1_min = std::stoi(first_elf[0]), elf1_max = std::stoi(first_elf[1]);
        int elf2_min = std::stoi(second_elf[0]), elf2_max = std::stoi(second_elf[1]);


        if( (elf1_min >= elf2_min && elf1_min <= elf2_max) || 
            (elf1_max >= elf2_min && elf1_max <= elf2_max) ||
            (elf2_min >= elf1_min && elf2_min <= elf1_max) || 
            (elf2_max >= elf1_min && elf2_max <= elf1_max)) {
            result++;
        }
    }

    std::cout << result;

    file.close();
    return 0;
}
