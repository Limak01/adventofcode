#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <stack>

#define MAX_DIR_SIZE 100000
#define UPDATE_SIZE 30000000


std::vector<std::string> split(std::string s, char delim) {
    std::vector<std::string> result;
    std::string s_slice = "";
    int s_len = s.length();

    for(int i = 0; i < s_len; i++) {
        if(s[i] == delim) {
            result.push_back(s_slice);
            s_slice = "";
            continue;
        }
    
        s_slice += s[i];
    }

    result.push_back(s_slice);

    return result;
}

struct Node {
    std::string type;
    std::string name;
    int size;
    Node* parent;
    std::vector<Node*> sub_dirs;
};

class FileSystem {
    Node* root;
    std::stack<Node*> curr_dir;
    int total_space = 70000000;

    public:
        FileSystem() {
            Node* tmp = new Node;
            tmp->type = "dir";
            tmp->size = 0;
            tmp->name = "/";
            tmp->parent = nullptr;
            root = tmp;
            curr_dir.push(tmp);
        }

        void mknode(std::string type, std::string name, int size) {
            if(name == root->name) return;

            Node* node = new Node;
            node->type = type;
            node->name = name;
            node->size = size;
            node->parent = curr_dir.top();
            curr_dir.top()->sub_dirs.push_back(node);
            

            //Update parent sizes
            Node* tmp = node->parent;
            while(tmp != nullptr) {
                tmp->size += node->size;
                tmp = tmp->parent;
            }
        }

        void cd(std::string dir) {
            if(dir == root->name) return;

            if(dir == "..") {
                curr_dir.pop(); 
            } else {
                Node* curr = curr_dir.top();

                for(Node* d: curr->sub_dirs) {
                    if(d->name == dir) {
                        curr_dir.push(d);
                    }
                }
            }
        }

        // Goes directly to root dir
        void cd() {
            while(curr_dir.top()->name != "/") {
                curr_dir.pop(); 
            }
        }

        void ls() {
            Node* curr = curr_dir.top();
            
            for(Node* dir: curr->sub_dirs) {
                if(dir->type == "dir")
                    std::cout << dir->type << " ";
                else 
                    std::cout << dir->size << " ";

                std::cout << dir->name << std::endl;
            }
        }

        void pwd() {
            std::cout << "You are currently in: " << curr_dir.top()->name << std::endl;
        }

        int used_space() {
            return root->size;
        }

        int free_space() {
            return total_space - root->size;
        }

        int max_space() {
            return total_space;
        }
        
        Node* get_root() {
            return root;
        }
};


//Part one
void sum_dirs(Node* root, int& sum) {
    if(root->size < MAX_DIR_SIZE) {
        sum += root->size;
    }

    for(Node* node: root->sub_dirs) {
        if(node->type == "dir") {
            sum_dirs(node, sum);
        }
    }
}

//Part two
void get_dirs_to_delete(Node* node, int curr_free_space, std::unordered_map<std::string, int>& dirs_to_delete) {
    if(node->size + curr_free_space >= UPDATE_SIZE) {
        dirs_to_delete[node->name] = node->size;
    }

    for(Node* d: node->sub_dirs) {
        if(d->type == "dir") {
            get_dirs_to_delete(d, curr_free_space, dirs_to_delete);
        }
    }
}

int main(int argc, char** argv) {
    std::fstream file;
    std::string line;
    FileSystem sys;
    file.open("input.txt", std::ios::in);

    while(getline(file, line)) {
        std::vector<std::string> splitted = split(line, ' '); 
        if(splitted[1] == "cd") {
            sys.cd(splitted[2]);
        }

        if(splitted[0] == "dir") {
            sys.mknode(splitted[0], splitted[1], 0);
        } else if(splitted[0] != "$") {
            sys.mknode("file", splitted[1], std::stoi(splitted[0]));
        }
    }
    sys.cd();
    
    // Part one
    //int part_one_result = 0;
    //sum_dirs(sys.get_root(), part_one_result);
    

    // Part two
    std::unordered_map<std::string, int> dirs_to_delete;
    int smallest = 0;
    get_dirs_to_delete(sys.get_root(), sys.free_space(), dirs_to_delete);

    for(auto x: dirs_to_delete) {
        if(smallest == 0) {
            smallest = x.second;
            continue;
        }

        if(x.second < smallest) {
            smallest = x.second;
        }
    }
    

    std::cout << "Smallest dir size: " << smallest << std::endl;
    file.close();

    return 0;
}
