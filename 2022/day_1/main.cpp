#include <iostream>
#include <fstream>

int main(int argc, char** argv) {
    std::fstream file;
    std::string line;
    int first, second, third, curr;

    first = second = third = curr = 0;

    file.open("input.txt", std::ios::in);
    
    while(getline(file, line)) {
        if(line != "") {
            curr += std::stoi(line);
        } else {
            if(curr > first) {
                third = second;
                second = first;
                first = curr;
            }

            if(curr < first && curr > second) {
                third = second;
                second = curr;
            }

            if(curr < second && curr > third) {
                third = curr;
            }

            curr = 0;
        }
    };


    int sum = first + second + third;

    std::cout << "First: " << first << std::endl;
    std::cout << "Second: " << second << std::endl;
    std::cout << "Third: " << third << std::endl;
    std::cout << "Sum: " << sum << std::endl;
    

    file.close();
    
    return 0;
}
