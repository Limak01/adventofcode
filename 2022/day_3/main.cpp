#include <iostream>
#include <vector>
#include <unordered_map>
#include <fstream>

int get_item_priority(char item) {
    if(item >= 'a' && item <= 'z') {
        return item - 'a' + 1;
    } 

    return item - 'A' + 26 + 1;
}

int get_priority_of_same_items(std::string s) {
    int s_len = s.length();
    std::unordered_map<char, int> mp;
    std::vector<char> items;

    for(int i = 0; i < s_len; i++) {
        if(i < s_len / 2) {
            mp[s[i]] = 0;
            continue;
        }

        if(mp.find(s[i]) != mp.end()) {
            return get_item_priority(s[i]);
        }
    }

    return 0;
}

char get_same_item(std::string arr[]) {
    std::unordered_map<char, int> c_count;

    for(char c: arr[0]) {
        c_count[c] = 0;
    }

    for(int i = 1; i < 3; i++) {
        for(char c: arr[i]) {
            if(c_count.find(c) != c_count.end()) {
                if(c_count[c] >= i - 1 && c_count[c] < i)
                    c_count[c]++;
            }
        }
    }

    for(std::pair<char, int> item: c_count) {
        if(item.second == 2) {
            return item.first;
        }
    }

    return -1;
}

int main(int argc, char** argv) {
    std::fstream file; 
    int sum = 0, count_lines = 0;
    std::string line;
    std::string lines[3];
        
    file.open("input.txt", std::ios::in);
    
    while(getline(file, line)) {
    
        lines[count_lines] = line;
        count_lines++;

        if(count_lines == 3) { 
            count_lines = 0;
            char same_c = get_same_item(lines);
            sum += get_item_priority(same_c);
        };
    }
    
    std::cout << sum << std::endl;

    file.close();
    return 0;
}
