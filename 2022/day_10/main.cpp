#include <iostream>
#include <fstream>
#include <vector>

std::vector<std::string> split(std::string s, char delim) {
    int s_len = s.length();
    std::string tmp = "";
    std::vector<std::string> result;

    for(int i = 0; i < s_len; i++) {
        if(s[i] == delim) {
            result.push_back(tmp);
            tmp = "";
            continue;
        }

        tmp += s[i];
    }

    result.push_back(tmp);

    return result;
}

bool check_cycle(int cycle) {
    if(cycle == 20) {
        return true;
    } else if(cycle == 60) {
        return true;
    } else if(cycle == 100) {
        return true;
    } else if(cycle == 140) {
        return true;
    } else if(cycle == 180) {
        return true;
    } else if(cycle == 220) {
        return true;
    }

    return false;
}

std::vector<std::vector<char>> init_screen() {
    std::vector<std::vector<char>> result;
    
    for(int i = 0; i < 6; i++) {
        std::vector<char> row;
        for(int j = 0; j < 40; j++) {
            row.push_back('.'); 
        }
        result.push_back(row);
    }

    return result;
}

void print_screen(std::vector<std::vector<char>> screen) {
    for(auto row: screen) {
        for(auto c: row) {
            std::cout << c;
        }

        std::cout << std::endl;
    }
}


void draw(std::vector<std::vector<char>>& screen, int row, int pixel, int x) {
    if(pixel == x || pixel == x - 1 || pixel == x + 1) {
        screen[row][pixel] = '#';
    }
}

void make_cycle(int& x, int& cycle, int& sum, int& row, int& pixel, std::vector<std::vector<char>>& screen) {
    cycle++;

    if(check_cycle(cycle)) sum += x * cycle;
    draw(screen, row, pixel, x);
    pixel++;

    if(pixel == 40) {
        pixel = 0;
        row++;
    }
}

int main(int argc, char** argv) {
    std::fstream file("input.txt", std::ios::in);
    std::string line;
    std::vector<std::vector<char>> screen = init_screen();
    int x = 1, cycle = 0, sum = 0, row = 0, pixel = 0;

    while(getline(file, line)) {
        std::vector<std::string> splitted = split(line, ' ');
        make_cycle(x, cycle, sum, row, pixel, screen);

        if(splitted[0] == "addx") {
            make_cycle(x, cycle, sum, row, pixel, screen);
            x += std::stoi(splitted[1]);
        }
    }

    print_screen(screen);
    std::cout << sum;
    file.close();
    return 0;
}
