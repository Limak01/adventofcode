#include <iostream>
#include <fstream>
#include <vector>

struct Direction {
    int x;
    int y;
};

// Part one
bool is_visible(std::vector<std::vector<int>> grid, Direction cords, Direction dir, int value, int& count) {
    int w = grid[0].size();
    int h = grid.size();
    
    if(cords.y == 0 || cords.y == h - 1 || cords.x == 0 || cords.x == w - 1) {
        if(grid[cords.y][cords.x] >= value) {
            return false;
        } else {
            return true;
        }
    }

    if(grid[cords.y][cords.x] >= value) {
        return false;
    }

    return is_visible(grid, { cords.x + dir.x, cords.y + dir.y }, dir, value, count);
}

// Part two
int get_count_of_trees(std::vector<std::vector<int>> grid, Direction cords, Direction dir, int value) {
    int w = grid[0].size();
    int h = grid.size();
    int count = 0;

    while((cords.y >= 0 && cords.y <= h - 1) && (cords.x >= 0 && cords.x <= w - 1)) {
        if(grid[cords.y][cords.x] >= value) {
            count++;
            break;
        }

        count++;

        cords.x = cords.x + dir.x;
        cords.y = cords.y + dir.y;
    }

    return count;
}

int main(int argc, char** argv) {
    std::fstream file;
    std::string line;
    std::vector<std::vector<int>> grid;

    file.open("input.txt", std::ios::in);
        
    while(getline(file, line)) {
        int line_length = line.length();
        std::vector<int> row;

        for(int i = 0; i < line_length; i++) {
            row.push_back(line[i] - '0');        
        }
        
        grid.push_back(row);
    }

    file.close();

    int w = grid[0].size();
    int h = grid.size();
    
    // Part one
    //std::vector<int> visible_trees;

    //for(int i = 1; i < h - 1; i++) {
    //    for(int j = 1; j < w - 1; j++) {
    //        if(is_visible(grid, { j, i -1 }, {0, -1},grid[i][j]) || 
    //           is_visible(grid, { j, i + 1} , {0, 1}, grid[i][j]) || 
    //           is_visible(grid, { j - 1 , i }, { -1, 0}, grid[i][j]) || 
    //           is_visible(grid, { j + 1, i } , { 1, 0},  grid[i][j])) {
    //            visible_trees.push_back(grid[i][j]);
    //        }
    //    }

    //}

    //std::cout << visible_trees.size() + (2 * (w - 2)) + (2 * h);
    //
    int highest = 0; 
    for(int i = 1; i < h - 1; i++) {
        for(int j = 1; j < w - 1; j++) {
            int top = get_count_of_trees(grid, { j, i - 1 }, {0, -1}, grid[i][j]); 
            int bottom = get_count_of_trees(grid, { j, i + 1 }, {0, 1}, grid[i][j]); 
            int left = get_count_of_trees(grid, { j - 1, i }, {-1, 0}, grid[i][j]); 
            int right = get_count_of_trees(grid, { j + 1, i }, {1, 0}, grid[i][j]);

            int score = top * bottom * left * right;

            if(score > highest) highest = score;
        }
    }

    std::cout << highest;

    return 0; 
}
