#!/usr/bin/env node
const fs = require("fs");

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, data) => {
    const d = data.split("\n");
    const nums_left = [];
    const nums_right = [];


    d.forEach((line) => {
        nums_left.push(parseInt(line.split("   ")[0]));
        nums_right.push(parseInt(line.split("   ")[1]));
    });

    nums_left.sort();
    nums_right.sort();

    let result = 0;

    for(let i = 0; i < nums_left.length; i++) {
        result += Math.abs(nums_left[i] - nums_right[i]);
    }

    console.log("Part one: ", result);
});

fs.readFile(`${__dirname}/input.txt`, 'utf8', (err, data) => {
    const d = data.split("\n");
    const nums_left = [];
    const nums_right = [];


    d.forEach((line) => {
        nums_left.push(parseInt(line.split("   ")[0]));
        nums_right.push(parseInt(line.split("   ")[1]));
    });


    let result = 0;

    for(let i = 0; i < nums_left.length; i++) {
        let count = 0;

        nums_right.forEach((number) => {
            if(number == nums_left[i]) count++;
        });

        result += count * nums_left[i];
    }

    console.log("Part two: ", result);
});