const fs = require("fs");
const input = fs.readFileSync("input.txt", "utf-8");
const lines = input.split("\n");

const rules = {};
const updates = [];

let ordering = true
for(const line of lines) {
    if(line == "") {
        ordering = false;
        continue;
    }

    if(ordering) {
        const orderingRule = line.split("|");
        rules[orderingRule[0]] === undefined && (rules[orderingRule[0]] = []);
        rules[orderingRule[0]].push(orderingRule[1]);
    } else {
        updates.push(line.split(","));
    }
}

function isValid(update) {
    for(const entry of update) {
        const entryRules = rules[entry];
        const entryIdx = update.indexOf(entry);

        if(entryRules !== undefined) {
            for (const rule of entryRules) {
                const ruleIdx = update.indexOf(rule);
                if (ruleIdx !== -1 && entryIdx > ruleIdx) {
                    return false;
                }
            }
        }
    }

    return true;
}

function isEntryValid(entry, update) {
    const entryRules = rules[entry];
    if (entryRules !== undefined) {
        for (const rule of rules[entry]) {
            const ruleIdx = update.indexOf(rule);
            if (ruleIdx !== -1 && update.indexOf(entry) > ruleIdx) {
                return false;
            }
        }

        return true;
    } else {
        return false;
    }
}

//Part one
let result = 0;
for(const update of updates) {
    if(isValid(update)) {
        result += parseInt(update[Math.floor(update.length / 2)]);
    }
}

console.log("Part one:", result);


//Part two
let result2 = 0;
for(const update of updates) {
    while (!isValid(update)) {
        for (const entry of update) {
            if (!isEntryValid(entry, update)) {
                const entryIdx = update.indexOf(entry);
                const entryRules = rules[entry];

                if (entryRules != undefined) {
                    for (const rule of entryRules) {
                        const ruleIdx = update.indexOf(rule);
                        if (ruleIdx != -1 && entryIdx > ruleIdx) {
                            const tmp = update[entryIdx];
                            update[entryIdx] = update[ruleIdx];
                            update[ruleIdx] = tmp;
                            break;
                        }
                    }
                }
            }
        }
    }

    if(isValid(update)) {
        result2 += parseInt(update[Math.floor(update.length / 2)]);
    }
}

console.log("Part two:", result2 - result);