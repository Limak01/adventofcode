const fs = require("fs");

const input = fs.readFileSync("input.txt", "utf-8");

const lines = input.split("\n");

let safe = 0;
let safe_part2 = 0;

function isSafe(numbers) {
    const increment = numbers[0] < numbers[1];

    for(let i = 1; i < numbers.length; i++) {
        let diff = increment ? numbers[i] - numbers[i - 1] : numbers[i - 1] - numbers[i];

        if(diff < 1 || diff > 3) return false;
    }

    return true;
}

//Part one
lines.forEach(line => {
    const numbers = line.split(" ").map(str => parseInt(str));

    if(isSafe(numbers)) safe++;
});

//Part two
lines.forEach(line => {
    const numbers = line.split(" ").map(str => parseInt(str));

    if (isSafe(numbers)) {
        safe_part2++;
    } else {
        for (let i = 0; i < numbers.length; i++) {
            const nums = numbers.slice(0, i).concat(numbers.slice(i + 1));

            if(isSafe(nums)) {
                safe_part2++;
                break;
            }
        }
    }

});

console.log(safe_part2);