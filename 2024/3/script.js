const fs = require("fs");
const input = fs.readFileSync("input.txt", "utf-8");

const rgx = /mul\((\d+),(\d+)\)/g;
const rgx2 = /don't|do|mul\((\d+),(\d+)\)/g;

//PART ONE

let match;
let result = 0;

while((match = rgx.exec(input)) !== null) {
    result += parseInt(match[1]) * parseInt(match[2]);
}

console.log("Part 1:", result);

//PART TWO
let match2
let instruction = true; //true means do!
let result2 = 0;

while((match2 = rgx2.exec(input)) !== null) {
    if(match2[0] === "don't") instruction = false;
    else if(match2[0] === "do") instruction = true;
    else {
        if(instruction) {
            result2 += parseInt(match2[1]) * parseInt(match2[2]);
        }
    }
}

console.log("Part 2:", result2);



