const fs = require('fs');
const input = fs.readFileSync("input.txt", "utf-8");
const lines = input.split("\n");



function checkForXMAS(arr, windex, word, x, y, dir) {
    if(windex === word.length) return 1;

    if(x >= arr[0].length || x < 0 || y >= arr.length || y < 0) {
        return 0;
    }

    if(arr[y][x] !== word[windex]) return 0;

    return checkForXMAS(arr, windex + 1, word, x + dir.x, y + dir.y, dir); 

}


//PART ONE
let count = 0;
const dirs = [{ x: 1, y: 0 }, { x: -1, y: 0 }, { x: 0, y: -1 }, { x: 0, y: 1 }, { x: 1, y: 1 }, { x: 1, y: -1 }, { x: -1, y: -1 }, { x: -1, y: 1 }];

for(let i = 0; i < lines.length; i++) {
    for(let j = 0; j < lines[0].length; j++) {
        if(lines[i][j] === 'X') {
            for(let dir of dirs) {
                count += checkForXMAS(lines, 0, "XMAS", j, i, dir);
            }
        }
    }
}

console.log("Part one:", count);

//PART TWO

let count2 = 0;
const dirs2 = [{x: 1, y: 1}, {x: -1, y: 1}, {x: 1, y: -1}, {x: -1, y: -1}];

for(let i = 0; i < lines.length; i++) {
    for(let j = 0; j < lines[0].length; j++) {
        if(lines[i][j] === 'A') {
            let total = 0;

            for(let dir of dirs2) {
            total += checkForXMAS(lines, 0, "MAS", j + (dir.x * -1), i + (dir.y * -1), dir);
            }

            total === 2 && count2++;
        }
    }
}

console.log("Part two:", count2);